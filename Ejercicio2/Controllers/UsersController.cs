﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.RegularExpressions;
using System.Web.Http;

namespace Ejercicio2.Controllers
{
    public class UsersController : ApiController
    {
       
        public IEnumerable<Models.Usuarios> Get(string searchby)
        {
            IEnumerable<Models.Usuarios> listaUser = null;
            using (Models.Ejercicio2Entities db = new Models.Ejercicio2Entities())
            {
                db.Configuration.ProxyCreationEnabled = false;
                //incluye entidades relacionadas
                listaUser = db.Usuarios.Include("Profile1").Include("Workshift1").Where(element => element.Rfc.Contains(searchby) || element.Name.Contains(searchby)).ToList ();
            }
            return listaUser;

        }
        [Route("api/Users/{searchby}/{searchby2?}")]
        public IEnumerable<Models.Usuarios> Get(string searchby,string searchby2=null)
        {
            IEnumerable<Models.Usuarios> listaUser = null;
            using (Models.Ejercicio2Entities db = new Models.Ejercicio2Entities())
            {
                //incluye entidades relacionadas
                listaUser = db.Usuarios.Include("Profile1").Include("Workshift1").Where(element => element.Rfc.Contains(searchby) || element.Name .Contains(searchby)|| element.Rfc.Contains(searchby2) || element.Name.Contains(searchby2)).ToList();
            }
            return listaUser;

        }

        public Models.Users Post(Models.Users userParam)
        {
            Models.Users user = new Models.Users();
            var responseMsj = new HttpResponseMessage(HttpStatusCode.OK);

            HttpResponseMessage responseCreated = Request.CreateResponse(HttpStatusCode.Created);

            if (validaRFC(userParam.RFC))
            {
                if (ValidarTelefono(userParam.Phone))
                {

                    using (Models.Ejercicio2Entities db = new Models.Ejercicio2Entities())
                    {

                        //Obtiene la cantidad de usuarios con el PIN que se ingresó
                        var userFindUnic = db.Users.Where(element => element.PIN  == userParam.PIN).Count ();
                        if (userFindUnic ==0)
                        {
                            //Valida que si se ingresó un key user, si es así es un insert de lo contrario es un update
                            if (userParam.KeyUser == 0)
                            {
                                user.Name = userParam.Name;
                                user.Lastname = userParam.Lastname;
                                user.Surname = userParam.Surname;
                                user.RFC = userParam.RFC;
                                user.Workshift = userParam.Workshift;
                                user.Gender = userParam.Gender;
                                user.Phone = userParam.Phone;
                                user.PIN = userParam.PIN;
                                try {
                                    //  Asigna formato de fecha
                                    DateTime oDate = DateTime.ParseExact(userParam.Birthdate, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
                                    user.Birthdate = oDate.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
                                    db.Users.Add(user);
                                    db.SaveChanges();
                                    db.Dispose();
                                    
                                    var id = user.KeyUser.ToString();                                   
                                        
                                    responseCreated.Headers.Add("Id", id);
                                    return user;//ResponseMessage(responseCreated);
                                } catch (Exception ex) {
                                    //Notifica formato incorrecto
                                    var message = string.Format("El formato de fecha es incorrecto, debería de ser yyyyMMdd");
                                    throw new HttpResponseException(
                                        Request.CreateErrorResponse(HttpStatusCode.BadRequest, message));
 
                                }
                                
                            }
                            else
                            {
                                //valida si ya existe un KeyUser
                                var userFind = db.Users.Where(element => element.KeyUser == userParam.KeyUser);
                                if (userFind != null)
                                {
                                    user = userFind.SingleOrDefault();
                                    user.Name = userParam.Name;
                                    user.Lastname = userParam.Lastname;
                                    user.Surname = userParam.Surname;
                                    user.RFC = userParam.RFC;
                                    user.Workshift = userParam.Workshift;
                                    user.Gender = userParam.Gender;
                                    user.Phone = userParam.Phone;
                                    user.PIN = userParam.PIN;

                                    try
                                    {
                                        DateTime oDate = DateTime.ParseExact(userParam.Birthdate, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
                                        user.Birthdate = oDate.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
                                        db.SaveChanges();

                                        HttpResponseMessage responseFecha = Request.CreateResponse(HttpStatusCode.OK , db);
                                        responseFecha.Headers.Add("Id", user.KeyUser.ToString());
                                    }
                                    catch (Exception ex)
                                    {
                                        //Notifica formato incorrecto
                                        var message = string.Format("El formato de fecha es incorrecto, debería de ser yyyyMMdd");
                                        throw new HttpResponseException(
                                            Request.CreateErrorResponse(HttpStatusCode.BadRequest, message));

                                    }

                                   


                                }
                            }
                        }
                        else {
                           // Notifica PIN repetido
                            var message = string.Format("El PIN debe de ser único");
                            throw new HttpResponseException(
                                Request.CreateErrorResponse(HttpStatusCode.BadRequest, message));


                        }


                    }
                    
                       
                }
                else {
                    //Notifica telefono inválido
                    var message = string.Format("El teléfono es inválido");
                    throw new HttpResponseException(
                        Request.CreateErrorResponse(HttpStatusCode.BadRequest, message));

                }
                
            }
            else {


                responseCreated = Request.CreateResponse(HttpStatusCode.BadRequest);
                responseCreated.Headers.Add("Incorrecto", "El RFC es inválido");
            }

            return user;
            
        }
        /// <summary>
        /// Valida el RFC ingresado
        /// </summary>
        /// <param name="rfc">RFC</param>
        /// <returns>True si es correcto o falso si es incorrecto</returns>
        public Boolean validaRFC(string rfc) {
            //El Registro Federal de Causantes (RFC)  JRH 9210065M1 
            // Si quieres que acepte mayuscula y minuscula [a-zA-Z]
            if (Regex.IsMatch(rfc, @"^([A-Z\s]{4})\d{6}([A-Z\w]{3})$"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// Valida el telefono
        /// </summary>
        /// <param name="strNumber">Telefono ingresado</param>
        /// <returns>True correto; false incorrecto</returns>
        public static bool ValidarTelefono(string strNumber)
        {
            Regex regex = new Regex(@"\A[0-9]{10}\z");
            Match match = regex.Match(strNumber);

            if (match.Success)
                return true;
            else
                return false;
        }

    }
}
